extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var camera = get_tree().get_root().get_node("Node").get_node("Camera");
var motion = Vector2()
var enablePhysics=true;
var grav = 8;
var hitTimer=null
var hitTime=2.0
var jumpTimer=null
var jumpTime=1.0
var isHit=false
var scaleVel=Vector2()
var originalScale=Vector2()
onready var flash = load("res://Flash.tscn")
var player
func _ready():
	originalScale=scale
	hitTimer = Timer.new()
	hitTimer.set_wait_time(hitTime)
	hitTimer.connect("timeout",self,"on_timeout_complete")
	add_child(hitTimer);
	jumpTimer=Timer.new()
	jumpTimer.set_wait_time(get_jump_time())
	jumpTimer.connect("timeout",self,"on_jump_timeout")
	jumpTimer.start()
	add_child(jumpTimer);
	player = get_tree().get_root().get_node("Node").get_node("Player")
	pass # Replace with function body.
func get_jump_time():
	return rand_range(jumpTime*0.5,jumpTime*1.5)
func on_jump_timeout():
	if is_on_floor() and !isHit:
		motion.y=-300;
		jumpTimer.set_wait_time(get_jump_time());

func on_timeout_complete():
	isHit=false
	grav=8
	remove_collision_exception_with(player);


func hit(other,otherFacingRight):
	if(!isHit):
		scale = Vector2(originalScale.x*0.5,originalScale.y*1.5)
		var flash_instance = flash.instance()
		get_tree().get_root().add_child(flash_instance)
		flash_instance.position=position
		flash_instance.play()
		flash_instance.set_rotation(rand_range(0,360))
		camera.ScreenShake(10, 1)
		get_tree().paused=true
		yield(get_tree().create_timer(0.3), 'timeout')
		get_tree().paused=false
		$Sprite.play("hit")
		isHit=true
		grav=20
		add_collision_exception_with(player);
		print("hit")
		motion.y=0
		var otherToThis = (position-other.position);
		otherToThis.y=0;
		if(otherFacingRight):
			otherToThis.x=1
		else:
			otherToThis.x=-1
		if(otherToThis.y<1):
			motion=(otherToThis.normalized()+Vector2(0,-1))*400
		else:
			motion=(otherToThis.normalized()+Vector2(0,-1))*400
		hitTimer.start()

func _physics_process(delta):
	#print(motion);
	if(enablePhysics):
		print(scale)
		var out = Helper.Maths.Spring_vec2(scale, scaleVel, originalScale,0.01,25.0,delta)
		scale = out[0]
		scaleVel=out[1]
		move_and_slide(motion,Vector2(0,-1));
		if(!isHit):
			var direction = player.position-position
			motion.x=lerp(motion.x,direction.x,0.1)
			if is_on_floor():
				motion.y=grav;
			else:
				motion.y+=grav;
		else:
			if !is_on_floor():
				motion.y+=grav;
	#for body in bodies:
		#print(body);
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if(body.is_in_group("Enemy") and !isHit):
		var away=-(body.position-position).normalized()*200
		away.y=0
		motion+=away
	pass # Replace with function body.
