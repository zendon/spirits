extends KinematicBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
var motion = Vector2();
var grav=20;
var enablePhysics=true;
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	#print(motion);
	if(enablePhysics):
		move_and_slide(motion,Vector2(0,-1));
		if is_on_floor():
			motion.y=grav;
		else:
			motion.y+=grav;
	#for body in bodies:
		#print(body);
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
	pass



func _on_Area2D_area_entered(area):

	pass

func reparent(newParent, child):
	var old_position = child.global_position
	child.get_parent().remove_child(child)
	newParent.add_child(child)
	child.global_position = old_position
	pass

func _on_Area2D_body_entered(body):
	print("hi")
	if body.is_in_group("Player"):
		reparent(body,self);
		enablePhysics=false
	pass
