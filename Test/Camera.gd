extends Camera2D

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

var screenShakeTimer = 0
var screenShakeLimit = 0
var screenShakeIntensity = 0


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	
	
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if(screenShakeTimer < screenShakeLimit):
		screenShakeTimer += delta;
		var shakeRange = Vector2(rand_range(-1, 1), rand_range(-1, 1));
		offset = shakeRange * screenShakeIntensity * ((screenShakeLimit - screenShakeTimer)  / screenShakeLimit);
	
	#shake_x = lerp(shake_x,0, 4* delta);
	#shake_y = lerp(shake_y,0, 4* delta);
	
	#var spr = Spring(shake_x, );
	
	pass

func ScreenShake(intensity:float, time:float):
	if(intensity > screenShakeIntensity):
		screenShakeIntensity = intensity;
	screenShakeLimit = time;
	screenShakeTimer = 0;
	pass