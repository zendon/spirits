extends Node

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
 
static func Spring(var x, var v, var xt, var zeta, var omega, var h):
	var f = 1.0 + 2.0 * h * zeta * omega;
	var oo = omega * omega;
	var hoo = h * oo;
	var hhoo = h * hoo;
	var detInv = 1.0 / (f + hhoo);
	var detX = f * x + h * v + hhoo * xt;
	var detV = v + hoo * (xt - x);
	
	var newX = detX * detInv;
	var newV = detV * detInv;
	
	return [newX, newV]
	
static func Spring_vec2(var vec, var vel, var target, var zeta, var omega, var h):
	var var1 = Spring(vec.x, vel.x, target.x, zeta, omega, h);
	var var2 = Spring(vec.y, vel.y, target.y, zeta, omega, h);
	
	var retVec = Vector2(var1[0], var2[0])
	var retVel = Vector2(var1[1], var2[1])
	
	return [retVec,retVel]

