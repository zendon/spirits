extends Sprite

# class member variables go here, for example:

export var speed = 30;
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	var input = Vector2(0,0);

	if(Input.is_action_pressed("ui_right")):
		input.x=1
	if(Input.is_action_pressed("ui_left")):
		input.x=-1

	if(Input.is_action_pressed("ui_up")):
		input.y=-1
	if(Input.is_action_pressed("ui_down")):
		input.y=1

	translate(input*speed*delta);
	get_node("Sprite").position.x=int(position.x);
	get_node("Sprite").position.y=int(position.y);
	pass
