extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

onready var camera = get_tree().get_root().get_node("Node").get_node("Camera");

var defaultSprite = preload("res://Person.png")
var kickSprite = preload("res://PersonKick.png")
var input = Vector2()
var motion = Vector2()
var speed=200
var grav=20
var isAttacking=false
var attackTime=0.3
var attackTimer
var prevInput
var prevMotion
var prevOnFloor


func _ready():
	attackTimer=Timer.new()
	attackTimer.set_wait_time(attackTime)
	attackTimer.one_shot=true;
	attackTimer.connect("timeout",self,"attack_timer_complete")
	add_child(attackTimer)
	
	
	
func hitWall():
	if(motion.y<0):
		motion.y=0;
	motion.y+=grav

func attack_timer_complete():
	if isAttacking:
		isAttacking=false
		$Sprite.play("default")
func Pause(time):
		yield(get_tree().create_timer(time), 'timeout')
func _physics_process(delta):
	# Called when the node is added to the scene for the first time.
	# Initialization here
	input=Vector2(0,0);
	if Input.is_action_pressed("ui_right"):
		input.x=1
	if Input.is_action_pressed("ui_left"):
		input.x=-1
	if Input.is_action_pressed("ui_up"):
		input.y=-1;
	if Input.is_action_pressed("ui_down"):
		input.y=1;



	if Input.is_action_just_pressed("ui_attack"):
		isAttacking=true;
		$Sprite.play("attack")
		attackTimer.start()

	if !isAttacking:
		motion.x=input.x*speed
		motion.y+=grav
	else:
		if prevMotion.x!=0:
			motion = prevMotion.normalized()*speed*2
		else:
			if $Sprite.flip_h:
				motion.x=-speed
			else:
				motion.x=speed
		motion.y=0
	if is_on_floor():
		if !prevOnFloor:
			$Sprite.play("land")
		if(input.y<0):
			motion.y=-400
		else:
			motion.y=grav

	if !isAttacking:
		if motion.x<0:
			$Sprite.flip_h = true
		elif input.x>0:
			$Sprite.flip_h = false

	var prevPos = position;
	prevOnFloor=is_on_floor()
	var remainder = move_and_slide(motion,Vector2(0,-1))
	var totalRemainder=Vector2();

	for i in range(0,get_slide_count()):
		var collision = get_slide_collision(i)
		if(collision.collider is StaticBody2D):
			totalRemainder+=remainder.slide(collision.normal)

	for i in range(0,get_slide_count()):
		var collision = get_slide_collision(i)
		if(collision.collider is preload("Enemy.gd")):
			print(position.y-collision.collider.position.y)
			if isAttacking: #or (position.y-collision.collider.position.y<-1 and collision.normal.y<0):
				var enemy = collision.collider;
				enemy.hit(self,!$Sprite.flip_h)
	prevInput = input
	prevMotion=motion
			#enemy.motion+=(enemy.position-position).normalized();
			#collision.collider.move_and_slide(motion,Vector2(0,-1))
	#move_and_slide(totalRemainder);
	#var collision = move_and_collide(motion*delta)
	#if(collision):
	#	var remainder =collision.remainder;
	#	if(collision.collider is KinematicBody2D):
	#		collision.collider.move_and_collide(remainder);
	#		move_and_collide(remainder);
	#		motion.y+=grav;
	#	else:
	#		translate(remainder.slide(collision.normal))
	#		if(input.y<0):
	#			if(collision.normal.y<-0.5):
	#				motion.y=-400;
	#			else:
	#				hitWall()
	#		else:
	#			if(collision.normal.y<-0.5):
	#				motion.y=0
	#			else:
	#				hitWall()
	#else:
	#	motion.y+=grav
	pass



func _on_Sprite_animation_finished():
	if $Sprite.animation=="land":
		$Sprite.play("default")
	pass # Replace with function body.
